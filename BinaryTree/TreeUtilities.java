import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Reflection:
 * This program was surprisingly pretty easy and enjoyable to make. It summed up a lot of concepts related to
 * tree traversal and forced me to read the tree display code which was also pretty enlightening in terms of
 * how trees are useful, what the pros and cons are for each. The morse code project overall was not that hard to
 * complete. I took a recursive approach but after thinking about it, an iterative 'for' loop wouldn't require
 * a helper method and would take up less space especially if morse code segments were longer, as recursive
 * elements stack on top of one another and only start getting carried out when the recursion ends, vs. for
 * loop which is printing out the results every iteration. However, the recursive approach was more logical
 * for me and helped me understand what I was doing better.
 */

//A container for useful static methods that operate on TreeNode objects.
public class TreeUtilities {
    //the random object used by this class
    private static java.util.Random random = new java.util.Random();

    //used to prompt for command line input
    private static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    //precondition:  t is non-empty
    //postcondition: returns the value in the leftmost node of t.
    public static Object leftmost(TreeNode t) {
        if (t == null) {
            return null;
        }
        TreeNode node = t;
        while (node.getLeft() != null) {
            node = node.getLeft();
        }
        return node.getValue();
    }

    //precondition:  t is non-empty
    //postcondition: returns the value in the rightmost node of t.
    public static Object rightmost(TreeNode t) {
        if (t == null) {
            return null;
        }
        TreeNode node = t;
        while (node.getRight() != null) {
            node = node.getRight();
        }
        return node.getValue();
    }

    //postcondition: returns the maximum depth of t, where an empty tree
    //               has depth 0, a tree with one node has depth 1, etc
    public static int maxDepth(TreeNode t) {
        if (t == null) {
            return 0;
        }
        int leftDepth = maxDepth(t.getLeft()) + 1;
        int rightDepth = maxDepth(t.getRight()) + 1;
        if (leftDepth > rightDepth) {
            return leftDepth;
        }
        return rightDepth;
    }

    //postcondition: each node in t has been lit up on display
    //               in a pre-order traversal
    public static void preOrder(TreeNode t, TreeDisplay display) {
        if (t == null) {
            return;
        }
        display.visit(t);
        preOrder(t.getLeft(), display);
        preOrder(t.getRight(), display);
    }

    //postcondition: each node in t has been lit up on display
    //               in an in-order traversal
    public static void inOrder(TreeNode t, TreeDisplay display) {
        if (t == null) {
            return;
        }
        inOrder(t.getLeft(), display);
        display.visit(t);
        inOrder(t.getRight(), display);
    }

    //postcondition: each node in t has been lit up on display
    //               in a post-order traversal
    public static void postOrder(TreeNode t, TreeDisplay display) {
        if (t == null) {
            return;
        }
        postOrder(t.getLeft(), display);
        postOrder(t.getRight(), display);
        display.visit(t);
    }

    //useful method for building a randomly shaped
    //tree of a given maximum depth
    public static TreeNode createRandom(int depth) {
        if (random.nextInt((int) Math.pow(2, depth)) == 0)
            return null;
        return new TreeNode(new Integer(random.nextInt(10)),
                createRandom(depth - 1),
                createRandom(depth - 1));
    }

    //returns the number of nodes in t
    public static int countNodes(TreeNode t) {
        if (t == null) {
            return 0;
        }
        return countNodes(t.getRight()) + countNodes(t.getLeft()) + 1;
    }

    //returns the number of leaves in t
    public static int countLeaves(TreeNode t) {
        if (t == null) {
            return 0;
        }
        if (t.getLeft() == null && t.getRight() == null) {
            return 1;
        }
        return countLeaves(t.getLeft()) + countLeaves(t.getRight());
    }

    //precondition:  all values in t are Integer objects
    //postcondition: returns the sum of all values in t
    public static int sum(TreeNode t) {
        if (t == null) {
            return 0;
        }
        return sum(t.getLeft()) + sum(t.getRight()) + (Integer) t.getValue();
    }

    //postcondition:  returns a new tree, which is a complete copy
    //                of t with all new TreeNode objects pointing
    //                to the same values as t (in the same order, shape, etc)
    public static TreeNode copy(TreeNode t) {
        if (t == null) {
            return null;
        }
        return new TreeNode(t.getValue(), copy(t.getLeft()), copy(t.getRight()));
    }

    //postcondition:  returns true if t1 and t2 have the same
    //                shape (but not necessarily the same values);
    //                otherwise, returns false
    public static boolean sameShape(TreeNode t1, TreeNode t2) {
        if (t1 == null && t2 == null) {
            return true;
        } else if (t1 != null && t2 == null) {
            return false;
        }
        if (t1 == null && t2 != null) {
            return false;
        }
        return sameShape(t1.getLeft(), t2.getLeft()) && sameShape(t1.getRight(), t2.getRight());
    }

    //postcondition:  returns a tree for decoding Morse code
    public static TreeNode createDecodingTree(TreeDisplay display) {
        TreeNode tree = new TreeNode("Morse Tree");
        display.displayTree(tree);
        insertMorse(tree, "a", ".-", display);
        insertMorse(tree, "b", "-...", display);
        insertMorse(tree, "c", "-.-.", display);
        insertMorse(tree, "d", "-..", display);
        insertMorse(tree, "e", ".", display);
        insertMorse(tree, "f", "..-.", display);
        insertMorse(tree, "g", "--.", display);
        insertMorse(tree, "h", "....", display);
        insertMorse(tree, "i", "..", display);
        insertMorse(tree, "j", ".---", display);
        insertMorse(tree, "k", "-.-", display);
        insertMorse(tree, "l", ".-..", display);
        insertMorse(tree, "m", "--", display);
        insertMorse(tree, "n", "-.", display);
        insertMorse(tree, "o", "---", display);
        insertMorse(tree, "p", ".--.", display);
        insertMorse(tree, "q", "--.-", display);
        insertMorse(tree, "r", ".-.", display);
        insertMorse(tree, "s", "...", display);
        insertMorse(tree, "t", "-", display);
        insertMorse(tree, "u", "..-", display);
        insertMorse(tree, "v", "...-", display);
        insertMorse(tree, "w", ".--", display);
        insertMorse(tree, "x", "-..-", display);
        insertMorse(tree, "y", "-.--", display);
        insertMorse(tree, "z", "--..", display);
        return tree;
    }

    //postcondition:  inserts the given letter into the decodingTree,
    //                in the appropriate position, as determined by
    //                the given Morse code sequence; lights up the display
    //                as it walks down the tree

    /**
     * The insertMorse method also uses traversal and the recursive method updates the tree every time
     * with the display.visit() method so that the newly inserted letter can be viewed right after
     * being added. The traversal works with a dot dash system where a dash makes the cursor go right
     * while the dot makes it go left.
     * @param decodingTree
     * @param letter
     * @param code
     * @param display
     */
    private static void insertMorse(TreeNode decodingTree, String letter, String code, TreeDisplay display) {
        display.visit(decodingTree);
        if (code.length() == 0) {
            decodingTree.setValue(letter);
        } else {
            char traversal = code.charAt(0);
            if (traversal == '-') {
                if (decodingTree.getRight() == null) {
                    decodingTree.setRight(new TreeNode(""));
                }
                insertMorse(decodingTree.getRight(), letter, code.substring(1), display);
            } else if (traversal == '.') {
                if (decodingTree.getLeft() == null) {
                    decodingTree.setLeft(new TreeNode(""));
                }
                insertMorse(decodingTree.getLeft(), letter, code.substring(1), display);
            } else {
                throw new IllegalStateException("No Such Character");
            }
        }
    }

    //precondition:  ciphertext is Morse code, consisting of dots, dashes, and spaces
    //postcondition: uses the given decodingTree to return the decoded message;
    //               lights up the display as it walks down the tree

    /**
     * The decodeMorse is the main method that really hands off the work to the decoder method.
     * It separates the cipherText into an array of separate characters using a very nifty split
     * method. Then, using an iterative for loop, the translated Morse Code is saved, returned
     * and printed out in the main method
     * @param decodingTree
     * @param cipherText
     * @param display
     * @return
     */
    public static String decodeMorse(TreeNode decodingTree, String cipherText, TreeDisplay display) {
        display.visit(decodingTree);
        String[] characters = cipherText.split(" ");
        String returnValue = "";
        for (String temp : characters) {
            returnValue += decoder(decodingTree, temp, display);
        }
        return returnValue;
    }

    /**
     * The decoder is simply a method that I used to assist me in the decoding process. It basically
     * takes in the morse segment and using a basic traversal algorithm, it searches out which
     * letter is being asked for based on a simple left/right system based off dots and dashes. It
     * then returns the right character by retrieving the value of the node it lands on after the
     * character list has run out of letters.
     * @param decodingTree
     * @param character
     * @param display
     * @return
     */
    private static String decoder(TreeNode decodingTree, String character, TreeDisplay display) {
        display.visit(decodingTree);
        if (character.length() == 0) {
            return decodingTree.getValue().toString();
        } else {
            char traversal = character.charAt(0);
            if (traversal == '-') {
                return decoder(decodingTree.getRight(), character.substring(1), display);
            } else if (traversal == '.') {
                return decoder(decodingTree.getLeft(), character.substring(1), display);
            } else {
                throw new IllegalArgumentException("No Such Character" + traversal);
            }
        }
    }

    //precondition:  expTree is an expression tree consisting of Integer objects
    //               joined by "+" and "*" operators
    //postcondition: returns the value of the expression tree
    public static int eval(TreeNode expTree) {
        throw new Error("Implement me!");
    }

    //precondition:  exp represents an arithmetic expression,
    //               consisting of "+", "*", paretheses and numbers
    //postcondition: returns an expression tree to represent this arithmetic expression
    public static TreeNode createExpressionTree(String exp) {
        throw new Error("Implement me!");
    }

    public static void main(String[] args) {
        TreeDisplay display = new TreeDisplay();
//        TreeNode tree = createRandom(4);
//        display.displayTree(tree);
//        preOrder(tree, display);
//        inOrder(tree, display);
//
//        System.out.println("Number of Nodes: " + countNodes(tree));
//        System.out.println("Number of Leaves: " + countLeaves(tree));
//        System.out.println("Sum of Tree Values: " + sum(tree));
//
//        display.displayTree(copy(tree));
//        System.out.println("Is the copy tree the same shape? (Expected: true) - " + sameShape(tree, copy(tree)));
//        TreeNode newTree = createRandom(3);
//        System.out.println("Is the new tree the same shape? (Expected: false) - " + sameShape(tree, newTree));
        TreeNode decodingTree = createDecodingTree(display);
        System.out.println("Message: " + decodeMorse(decodingTree, ". .- -", display));

    }
}
