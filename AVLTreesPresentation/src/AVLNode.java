public class AVLNode {
    public Comparable item;
    public AVLNode leftChild;
    public AVLNode rightChild;
    public int parentItem;

    public AVLNode(Comparable item, AVLNode leftChild, AVLNode rightChild, int parentItem) {
        this.item = item;
        this.leftChild = leftChild;
        this.rightChild = rightChild;
        this.parentItem = parentItem;
    }
}