# AP Multiple Choice Questions
AVL Trees

## Questions:
1. This type of tree is a self balancing tree that reorganizes itself if either the left or right subtrees differ by heights greater than one.
  * A) BSTree
  * B) AVL Trees
  * C) B-Trees
  * D) Heaps

  ANSWER: B. Of the four options, only AVL Trees reorganize themselves when the total heights of the subtrees differ by more than one. While some of the other trees also maintain some kind of level based or data based order, the AVL Tree is the only one that uses the height as an indicator of organization.

2. The method by which AVL Trees reorganize themselves is most commonly known as:
  * A) Reorder
  * B) Reconfigure
  * C) Rectify
  * D) Rotation

  ANSWER: D. When the AVL Tree detects that the difference in height between the two subtrees is too much, the AVL Tree calls upon the Rotation method to restore order.
