//Simple Java-based game that simulates a blackjack game.

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

/**
* This is the Blackjack class which simulates a blackjack game.
*
* The game involves one dealer and one player. The player starts with $1000 dollars and, after the first turn,
* is allowed to adjust his or her wager accordingly. The game is reset once the player loses all of his or her
* money and will continue a new game with parameters set to their original values.
*/
public class Blackjack {
  //Used Private Instance Variables
  private ArrayList<Card> deck;

  private HandComponent dealerHand;
  private HandComponent playerHand;

  private BufferedImage cardFaceImage;
  private BufferedImage cardBackImage;

  private JButton dealButton;
  private JButton hitButton;
  private JButton standButton;
  private JButton betButton;

  private JLabel instructionLabel;
  private JLabel scoreLabel;
  private JLabel betLabel;

  private int playerScore = 1000;
  private boolean inPlay = false;
  private int dealAmount = 10;

  /**
  * This extensive method creates all of the components that are to be seen on the board.
  * Included are the various buttons and the displays for the playing cards. Action Listeners
  * are also created accordingly as they are accessible to the player when appropriate.
  *
  * Additional methods such as the new game method and deal method are there to make the creation
  * and drawing of a deck easier and more efficient. The main method sets the frame and gives the
  * player a little pep talk before he or she begins on their greedy quest.
  */
  public void createComponents(Container containerPane) throws IOException {
    this.instructionLabel = new JLabel("Black Jack");
    containerPane.add(instructionLabel, BorderLayout.PAGE_START);

    /**
    * Button panel consists of buttons
    * The buttons are first instantiated as JButton objects.
    * They are then displayed by adding them to the button panel
    * Finally, they are assigned the action listener which defines what happens when the user clicks that button.
    */
    JPanel buttonPanel = new JPanel();

    this.hitButton = new JButton("Hit");
    buttonPanel.add(hitButton);
    this.hitButton.addActionListener(new HitActionListener());

    this.standButton = new JButton("Stand");
    buttonPanel.add(standButton);
    this.standButton.addActionListener(new StandActionListener());

    this.dealButton = new JButton("Deal");
    buttonPanel.add(dealButton);
    this.dealButton.addActionListener(new DealActionListener());

    this.betButton = new JButton("Change Bet");
    buttonPanel.add(betButton);
    this.betButton.addActionListener(new BetActionListener());

    this.betLabel = new JLabel("Bet Amount: $" + dealAmount);
    buttonPanel.add(betLabel);
    buttonPanel.setPreferredSize(new Dimension(150, 450));

    containerPane.add(buttonPanel, BorderLayout.WEST);

    //The image with all 52 cards on it.
    URL url = new URL("https://dl.dropboxusercontent.com/u/457924/cards.jfitz.png");
    this.cardFaceImage = ImageIO.read(url);
    url = new URL("https://dl.dropboxusercontent.com/u/457924/card_back.png");
    this.cardBackImage = ImageIO.read(url);

    //Cards panel
    this.dealerHand = new HandComponent(this.cardFaceImage, this.cardBackImage);
    final Dimension cardsDim = new Dimension(Card.CARD_SIZE[0] * 7, Card.CARD_SIZE[1]);
    dealerHand.setPreferredSize(cardsDim);
    dealerHand.setMinimumSize(cardsDim);

    this.playerHand = new HandComponent(this.cardFaceImage, this.cardBackImage);
    playerHand.setPreferredSize(cardsDim);
    playerHand.setMinimumSize(cardsDim);

    /**
    * weighty stands for spacing along the y axis
    * gridy specifies the cell at the top of the component display area
    */
    JPanel cardsPanel = new JPanel(new GridBagLayout());
    cardsPanel.setBackground(Color.cyan);
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.weighty = 0.5;
    gbc.gridy = 0;
    cardsPanel.add(new JLabel("Dealer Cards"), gbc);

    gbc.weighty = 1.0;
    gbc.gridy = 1;
    cardsPanel.add(dealerHand, gbc);

    gbc.weighty = 0.5;
    gbc.gridy = 2;
    cardsPanel.add(new JLabel("Player Cards"), gbc);

    gbc.weighty = 1.0;
    gbc.gridy = 3;
    cardsPanel.add(playerHand, gbc);

    containerPane.add(cardsPanel, BorderLayout.CENTER);

    this.scoreLabel = new JLabel("Score: " + playerScore);
    containerPane.add(this.scoreLabel, BorderLayout.PAGE_END);
  }

  //Used to print message dialogs
  private static Runnable showMessageDialog(final JComponent parent, final String message) {
    return new Runnable() {
      public void run() {
        JOptionPane.showMessageDialog(parent, message, "Black Jack", JOptionPane.PLAIN_MESSAGE);
      }
    };
  }

  /**
  * Action Listeners associated with their respective buttons contain commands to be executed once the user
  * clicks the respective button.
  *
  * To regulate which buttons can or cannot be clicked during the game, I used boolean variables
  * such as inPlay in this case to gauge the accessibility of various buttons during different stages of
  * the game. For example, the hit button cannot be clicked once the user clicks the stand button, and
  * can be clicked once again after the user clicks the deal button.
  */
  private class DealActionListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      newGame();

      System.out.println(dealAmount + " " + playerScore);
      System.out.println("Deal button clicked");
    }
  }

  private class BetActionListener implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
      if (inPlay) return;
      int sub = getDealAmount();
      if (sub <= playerScore && sub > 0) {
        dealAmount = sub;
        betLabel.setText("Bet Amount: $" + dealAmount);
      }
    }
  }

  public int getDealAmount() {
    return Integer.parseInt(JOptionPane.showInputDialog(playerHand, "How much do you want to bet", "Black Jack", JOptionPane.QUESTION_MESSAGE));
  }

  private class HitActionListener implements ActionListener {
    public void actionPerformed(ActionEvent actionEvent) {
      if (inPlay) {
        playerHand.hit(deal());
        if (playerHand.isBusted()) {
          inPlay = false;
          playerScore = playerScore - dealAmount;

          scoreLabel.setText("Score: " + playerScore);
          dealerHand.showCards();

          SwingUtilities.invokeLater(showMessageDialog(playerHand, "You busted. Loss"));
        }
        System.out.println("Hit button clicked");
      }
    }
  }

  private class StandActionListener implements ActionListener {
    public void actionPerformed(ActionEvent actionEvent) {
      if (!inPlay) {
        return;
      }
      inPlay = false;
      if (playerHand.getValue() == 21 && playerHand.getNoOfCard() == 2) {
        JOptionPane.showMessageDialog(playerHand, "Blackjack. Winner!");
        playerScore = playerScore + dealAmount * 2;

      } else {
        dealerHand.showCards();
        while (dealerHand.dealMoreCard(dealerHand.getValue(), playerHand.getValue())) {
          dealerHand.hit(deal());
        }
        if (dealerHand.getValue() > 21) {
          SwingUtilities.invokeLater(showMessageDialog(playerHand, "Dealer Busted. Winner!!!"));
          playerScore = playerScore + dealAmount;
        } else if (dealerHand.getValue() > playerHand.getValue()) {
          playerScore = playerScore - dealAmount;
          if (playerScore <= 0) {
            SwingUtilities.invokeLater(showMessageDialog(playerHand, "You bankrupted. Resetting game."));
            playerScore = 1000;
            dealAmount = 10;
            betLabel.setText("Bet Amount: $" + dealAmount);
          } else {
            SwingUtilities.invokeLater(showMessageDialog(playerHand, "Loss"));
          }
        } else if (dealerHand.getValue() == playerHand.getValue()) {
          SwingUtilities.invokeLater(showMessageDialog(playerHand, "Draw"));
        } else {
          SwingUtilities.invokeLater(showMessageDialog(playerHand, "Winner!!!"));
          playerScore = playerScore + dealAmount;
        }
      }
      scoreLabel.setText("Score: " + playerScore);
      System.out.println("Stand button clicked");
    }
  }

  /**
  * The newDeck method creates a new deck of cards. The method is used
  * in the newGame method, so all cards are taken away and reshuffled before
  * distribution.
  */
  public void newDeck() {
    deck = new ArrayList<Card>();
    for (int i = 0; i < 4; i++) {
      for (int j = 0; j <= 12; j++) {
        deck.add(new Card(j, i));
      }
    }
    Collections.shuffle(deck);
  }

  public Card deal() {
    return deck.remove(0);
  }

  /**
  * The newGame method reshuffles the deck and sets the inPlay status to true. Both the
  * player's and dealer's hands are reset and each receives two new cards.
  */
  public void newGame() {
    newDeck();
    Collections.shuffle(deck);
    inPlay = true;

    dealerHand.newGame();
    dealerHand.hit(deal());
    Card card = deal();
    card.setHidden(true);
    dealerHand.hit(card);

    playerHand.newGame();
    playerHand.hit(deal());
    playerHand.hit(deal());
  }

  /**
  * Main method. The JFrame is created and set. Before the actual game, the user is shown a quick message
  * that establishes the starting amount the player will have. Then the game begins.
  * @param args
  * @throws Exception
  */
  public static void main(String[] args) throws Exception {
    //create a frame or window
    JFrame frame = new JFrame();
    //set window size
    frame.setSize(500, 500);
    //set the title
    frame.setTitle("Black Jack");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JOptionPane.showMessageDialog(frame, "Welcome to Blackjack\nYou will start out with $1000\nHow high can you go?", "Black Jack", JOptionPane.PLAIN_MESSAGE);
    Blackjack blackJack = new Blackjack();
    blackJack.createComponents(frame.getContentPane());
    blackJack.newDeck();
    blackJack.newGame();

    frame.pack();
    frame.setVisible(true);
  }
}

/**
* The HandComponent class encompasses the methods and instance variables that make up a hand of cards.
* In the program, the class is used for the player and the dealer. Most of the methods involve manipulation
* of card that both access and mutate the amount of the hand. Other methods aid with the display of cards.
*/
class HandComponent extends JComponent {
  private ArrayList<Card> myCards;
  private BufferedImage cardFaceImage;
  private BufferedImage cardBackImage;

  public HandComponent(BufferedImage cardFaceImage, BufferedImage cardBackImage) {
    myCards = new ArrayList<Card>();
    this.cardFaceImage = cardFaceImage;
    this.cardBackImage = cardBackImage;
  }

  /**
  * Adds a card to the player's hand
  * @param card
  */
  public void hit(Card card) {
    myCards.add(card);
    repaint();
  }

  /**
  * Calculates whether the player's score is >21
  * @return true if busted
  */
  public boolean isBusted() {
    if (getValue() > 21) {
      return true;
    }
    return false;
  }

  @Override
  protected void paintComponent(Graphics graphics) {
    super.paintComponent(graphics);
    int x = 10;
    for (Card card : myCards) {
      if (card.isHidden()) {
        graphics.drawImage(cardBackImage, x, 0, null);
        x += cardBackImage.getWidth();
      } else {
        // get sub image for the card
        BufferedImage subImage = cardFaceImage.getSubimage(
        Card.CARD_SIZE[0] * card.getNumber(),
        Card.CARD_SIZE[1] * card.getSuit(),
        Card.CARD_SIZE[0],
        Card.CARD_SIZE[1]);
        graphics.drawImage(subImage, x, 0, null);
        x += Card.CARD_SIZE[0];
      }
      x += 10;
    }
  }

  public void newGame() {
    myCards.clear();
  }

  /**
  * Calculates the value of the cards in the player's hand.
  * The algorithm for aces is that if the current sum + ace > 21, then
  * the ace's value is 1. Otherwise, the program will return the value
  * with the ace with a value of 11.
  * @return Value of the cards in the hand.
  */
  public int getValue() {
    int sum = 0;
    int nAces = 0;
    for (Card card : myCards) {
      if (card.getNumber() >= 9)
      sum += 10;
      else {
        sum += card.getNumber() + 1;
      }
      if (card.getNumber() == 0) {
        nAces++;
      }
    }
    if (nAces == 0) {
      return sum;
    } else if (sum + 10 > 21) {
      return sum;
    } else {
      return sum + 10;
    }
  }

  /**
  * This is part of the algorithm for the dealer logic. If the amount of the
  * cards is below a threshold (in this case, 16), then the dealer will keep hitting.
  * The dealer will stop hitting when he either busts, achieves a value greater than the
  * player, or achieves a value greater than the threshold.
  * @param sum is the total value of the cards
  * @param playerScore the value of the player's cards after clicking stand
  * @return true if at least one of the three conditions aren't satisfied
  */
  public boolean dealMoreCard(int sum, int playerScore) {
    if (sum > 16 || sum > playerScore) {
      return false;
    }
    return true;
  }

  /**
  * Reveals all cards
  */
  public void showCards() {
    for (Card card : myCards) {
      card.setHidden(false);
    }
    repaint();
  }

  /**
  * Returns the number of cards in the deck
  * @return Number of cards in deck
  */
  public int getNoOfCard() {
    return myCards.size();
  }
}

/**
* The Card class defines the methods and variables associated with one card.
* The instance variables include the number and suit along with a boolean isHidden value
* that indicates whether the card is face up or face down.
*
* Other methods include printing methods, accessor methods, and card face manipulation methods
*/
class Card {
  private int number;
  private int suit;
  private boolean isHidden;
  static final int[] CARD_SIZE = {73, 98};

  public Card(int number, int suit) {
    this.number = number;
    this.suit = suit;
  }

  public int getNumber() {
    return number;
  }

  public int getSuit() {
    return suit;
  }

  public String toString() {
    String returnValue = number + " of " + suit;
    if (number == 1) {
      return "Ace of " + suit;
    }
    if (number == 11) {
      return "Jack of " + suit;
    }
    if (number == 12) {
      return "Queen of " + suit;
    }
    if (number == 13) {
      return "King of " + suit;
    }
    return returnValue;
  }

  public boolean isHidden() {
    return isHidden;
  }

  public void setHidden(boolean isHidden) {
    this.isHidden = isHidden;
  }
}
