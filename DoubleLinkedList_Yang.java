CODE
=======

/**
 * John Yang
 * 12/9/14
 * Period 5
 * Adv. Comp Sci - Data Structures
 */

import java.util.NoSuchElementException;

public class DoubleLinkedList_Yang<E> {
    private Node first;
    private Node last;

    private static DoubleLinkedList_Yang<Integer> createListOf3InOrder() {
        DoubleLinkedList_Yang<Integer> threeElementsInOrder = new DoubleLinkedList_Yang<Integer>();
        threeElementsInOrder.add(0, 5);
        threeElementsInOrder.add(1, 7);
        threeElementsInOrder.add(2, 3);
        return threeElementsInOrder;
    }

    public static void main(String[] args) {
        DoubleLinkedList_Yang<Integer> threeElementsInOrder = createListOf3InOrder();

        System.out.println("size() - Expected: 3  Actual: " + threeElementsInOrder.size());
        System.out.println("List - Expected: 5 7 3  Actual: " + threeElementsInOrder);
        System.out.println("getFirst() - Expected: 5  Actual: " + threeElementsInOrder.getFirst());
        System.out.println("getLast() - Expected: 3  Actual: " + threeElementsInOrder.getLast());

        threeElementsInOrder.addFirst(9);

        System.out.println("get(0) - Expected: 9  Actual: " + threeElementsInOrder.get(0));
        System.out.println("size() - Expected: 4  Actual: " + threeElementsInOrder.size());

        threeElementsInOrder = createListOf3InOrder();
        threeElementsInOrder.removeLast();
        System.out.println("size() - Expected: 2  Actual: " + threeElementsInOrder.size());
        System.out.println("List - Expected: 5 7  Actual: " + threeElementsInOrder);

        threeElementsInOrder = createListOf3InOrder();
        threeElementsInOrder.removeFirst();
        System.out.println("size() - Expected: 2  Actual: " + threeElementsInOrder.size());
        System.out.println("List - Expected: 7 3  Actual: " + threeElementsInOrder);

        threeElementsInOrder = createListOf3InOrder();
        threeElementsInOrder.remove(2);
        threeElementsInOrder.remove(0);
        threeElementsInOrder.remove(0);
        System.out.println("size() - Expected: 0  Actual: " + threeElementsInOrder.size());

        threeElementsInOrder = createListOf3InOrder();
        threeElementsInOrder.reverse();
        System.out.println("reverse() - Expected: 3 7 5  Actual: " + threeElementsInOrder);
    }

    private class Node {
        public E data;
        public Node next;
        public Node previous;
    }

    public DoubleLinkedList_Yang() {
        first = null;
        last = null;
    }

    private void enforceInvariants() {
        if (first == null) {
            last = null;
        } else if (last == null) {
            last = first;
        }
    }

    public E getFirst() {
        if (first == null)
            throw new NoSuchElementException();
        return first.data;
    }

    public E getLast() {
        if (first == null) {
            throw new NoSuchElementException();
        }
        return last.data;
    }

    public E removeFirst() {
        if (first == null)
            throw new NoSuchElementException();
        E element = first.data;
        Node next = first.next;
        first = first.next;
        if (next != null)
            next.previous = null;
        enforceInvariants();

        return element;
    }

    public E removeLast() {
        if (first == null)
            throw new NoSuchElementException();
        E element = last.data;
        last = last.previous;
        if (last == null) {
            first = last;
        } else {
            last.next = null;
        }
        enforceInvariants();

        return element;
    }

    public void addFirst(E element) {
        Node newNode = new Node();
        newNode.data = element;
        newNode.next = first;
        first = newNode;

        enforceInvariants();
    }

    public void addLast(E element) {
        Node newNode = new Node();
        newNode.data = element;
        if (first == null) {
            first = newNode;
        } else {
            newNode.previous = last;
            last.next = newNode;
            last = newNode;
        }
        enforceInvariants();
    }

    private Node getNodeAt(final int index) {
        int pos = 0;
        Node current = first;
        while (pos < index && current != null) {
            current = current.next;
            pos++;
        }

        if (pos == index && current != null) {
            return current;
        }

        throw new NoSuchElementException("Node at index " + index);
    }

    public E get(final int index) {
        Node node = getNodeAt(index);
        return node.data;
    }

    public void add(int index, E element) {
        if (index < 0) {
            throw new IllegalArgumentException("Argument not supported - index: " + index);
        }
        if (index == 0) {
            addFirst(element);
            return;
        }

        Node previous = getNodeAt(index - 1);
        Node successor = previous.next;

        if (successor == null) {
            addLast(element);
            return;
        }

        Node newNode = new Node();
        newNode.data = element;
        newNode.next = successor;
        newNode.previous = previous;

        previous.next = newNode;
        successor.previous = newNode;
    }

    public E remove(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("Argument not supported: " + index);
        }
        Node removed = getNodeAt(index);
        Node successor = removed.next;
        Node previous = removed.previous;
        if(previous == null) {
            return removeFirst();
        } else if (successor == null) {
            return removeLast();
        }
        previous.next = successor;
        successor.previous = previous;
        return removed.data;
    }

    public int size() {
        Node current = first;
        int count = 0;
        while (current != null) {
            current = current.next;
            count++;
        }
        return count;
    }

    public void reverse() {
        Node current = first;
        while(current != null) {
            Node oldPrevious = current.previous;
            Node oldNext = current.next;

            current.next = oldPrevious;
            current.previous = oldNext;

            current = oldNext;
        }
        Node firstTemp = first;
        Node lastTemp = last;
        first = lastTemp;
        last = firstTemp;
    }

    public String toString() {
        String returnValue = "";
        Node current = first;
        while (current != null) {
            returnValue = returnValue + " " + current.data;
            current = current.next;
        }
        return returnValue;
    }
}
