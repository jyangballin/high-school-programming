import java.util.ArrayList;
import java.util.Scanner;

class NewBattleship
{
    private String[][] field;
    private int Ax;
    private int Ay;

    public NewBattleship(int rows, int columns)
    {
        String[][] afield = new String[rows][columns];
        field = afield;

        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                field[i][j] = "X";
            }
        }
    }

    public int[][] placeShips()
    {
        int noOfShipSpots = 6;
        int coordinates = 2;
        int[][] locs = new int[noOfShipSpots][coordinates];
        int xCount = 0;
        int yCount = 0;

        ArrayList<String> locations = new ArrayList<String>();
        int columnNum = (int)(Math.random()*(field[0].length));
        int rowNum = (int)(Math.random()*(field.length - 2));

        field[rowNum][columnNum] = "A";
        locs[xCount][yCount] = rowNum;
        locs[xCount][yCount + 1] = columnNum;
        xCount++;

        locations.add("A1: " + columnNum + ", " + rowNum);
        field[rowNum + 1][columnNum] = "A";
        locs[xCount][yCount] = rowNum + 1;
        locs[xCount][yCount + 1] = columnNum;
        xCount++;

        field[rowNum + 2][columnNum] = "A";
        locs[xCount][yCount] = rowNum + 2;
        locs[xCount][yCount + 1] = columnNum;
        xCount++;

        boolean isConflict = true;
        int columnNumB = 0;
        int rowNumB = 0;
        while(isConflict == true)
        {
            columnNumB = (int)(Math.random()*(field[0].length - 1));
            rowNumB = (int)(Math.random()*(field.length));

            if(field[rowNumB][columnNumB].equals("A") || field[rowNumB][columnNumB + 1].equals("A"))
                isConflict = true;
            else
                isConflict = false;
        }

        field[rowNumB][columnNumB] = "B";
        locs[xCount][yCount] = rowNumB;
        locs[xCount][yCount + 1] = columnNumB;
        xCount++;

        field[rowNumB][columnNumB + 1] = "B";
        locs[xCount][yCount] = rowNumB;
        locs[xCount][yCount + 1] = columnNumB + 1;
        xCount++;

        int columnNumC = 0;
        int rowNumC = 0;
        isConflict = true;
        while(isConflict == true)
        {
            columnNumC = (int)(Math.random()*(field[0].length));
            rowNumC = (int)(Math.random()*(field.length));

            if(field[rowNumC][columnNumC].equals("A") || field[rowNumC][columnNumC].equals("B"))
                isConflict = true;
            else
                isConflict = false;
        }

        field[rowNumC][columnNumC] = "C";
        locs[xCount][yCount] = rowNumC;
        locs[xCount][yCount + 1] = columnNumC;

        return locs;
    }

    public void printField()
    {
        for (int i = 0; i < field.length; i++)
        {
            for (int j = 0; j < field[i].length; j++)
            {
                System.out.print(field[i][j] + " ");

            }
            System.out.println();
        }
    }

    public boolean hitOrMiss(int x, int y, int[][] locs)
    {
        for (int i = 0; i < locs.length; i++)
        {
            if(locs[i][0] == x && locs[i][1] == y)
            {
                String target = field[x][y];
                if(target.equals("A"))
                    System.out.println("You hit an Amphibious Assault Vehicle!");
                if(target.equals("B"))
                    System.out.println("You hit a Battleship!");
                if(target.equals("C"))
                    System.out.println("You hit an Aircraft Carrier!");
                return true;
            }
        }
        System.out.println("You missed!");
        return false;
    }
}

public class BattleshipTester
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Hello there and welcome to Battleship");
        System.out.println("Please enter the dimension of your field (Must be at least 3 x 3): ");
        int height = in.nextInt();
        int length = in.nextInt();

        NewBattleship game = new NewBattleship(height, length);
        int[][] shipLocations = game.placeShips();
        //game.printField();
        //printLocations(shipLocations);

        int hits = 0;
        int noOfShipSpots = 6;

        System.out.println("\nGame START");
        while(hits < noOfShipSpots)
        {
            System.out.println("Please enter two coordinates (x first, y second): ");
            int xCoord = in.nextInt();
            int yCoord = in.nextInt();
            boolean contact = game.hitOrMiss(xCoord, yCoord, shipLocations);

            if(contact == true)
            {
                hits++;
            }
        }
        System.out.println("\nYou hit all the ships! Good Game!");
    }

    public static void printLocations(int[][] field)
    {
        for (int i = 0; i < field.length; i++)
        {
            System.out.print("( ");
            for (int j = 0; j < field[i].length; j++)
            {
                System.out.print(field[i][j] + " ");
            }
            System.out.print(") ");
        }
    }
}
