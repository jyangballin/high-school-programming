//Import Statements
import java.util.*;
import java.util.LinkedList;

/**
 * This is the stack calculator I implemented. Using the shunting yard algorithm,
 * this class consists of three steps/methods to solve any equation input.
 * 1) tokenize - The method translates the equation into a list of strings
 * 2) convertToRpn - The method converts the list of strings into rpn, or reverse polish
 * notation based on a case-by-case approach of the operations and their precedence
 * 3) compute - The method takes the Rpn version of the equation and computes it.
 */
public class ShuntingYardCalculator {
    /**
     * This is essentially the tester class. The method allows a user to enter
     * a mathematical expression and the results will be produced.
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a mathematical expression: ");
        String equation = in.nextLine();
        ShuntingYardCalculator calc = new ShuntingYardCalculator();
        System.out.println("\nStep by Step:");
        System.out.println("\nAnswer: " + calc.evaluate(equation));
    }

    /**
     * The evaluate method basically puts the three steps of evaluating expressions
     * into order. It returns the result of the equation.
     */
    public int evaluate(String equation) {
        List<String> tokenList = tokenize(equation);
        JohnQueue<String> rpnQueue = convertToRpn(tokenList);
        return compute(rpnQueue);
    }

    /**
     * The tokenize method takes the string equation and takes the operators
     * and operands, placing them into a list of strings to be evaluated later.
     *
     * The method is capable of dealing with a lack of spaces with the tokenizer
     * inside the for loop. The StringTokenizer method needs a string parameter, but
     * if we take a closer look at the documentation, the stringtokenizer actually
     * calls upon another version of itself that asks for two more parameters, and by
     * calling upon that version of stringtokenizer, I can make it so that operator
     * symbols are treated as separator, and the boolean value tells the string tokenizer
     * to still include it in the tokenizer.
     *
     * In essence, the equation is broken up into pieces based on spaces, and then those
     * individuals pieces are reanalyzed with the upgraded stringtokenizer method to
     * see if numbers and operators were jumbled together.
     * @param equation the mathematical method.
     * @return a list containing individual operators and operands.
     */
    private List<String> tokenize(String equation) {
        List<String> tokenList = new ArrayList<String>();
        String[] tokens = equation.split("\\s");
        for (String token : tokens) {
            StringTokenizer tokenizer = new StringTokenizer(token, "+-*/^", true);
            while(tokenizer.hasMoreTokens()) {
                tokenList.add(tokenizer.nextToken());
            }
        }
        return tokenList;
    }

    /**
     * Converts the mathematical expression to rpn form. The mathematical expression,
     * which is already in a List form containing individual slots for the operators
     * and operands. Using the shouldPop method and isNumeric methods, the operators
     * and operands are reorganized into rpn form.
     * @param tokenList a list of operators and operands.
     * @return
     */
    private JohnQueue<String> convertToRpn(List<String> tokenList) {
        JohnQueue<String> queue = new JohnQueue<String>();

        JohnStack<Operator> operatorStack = new JohnStack<Operator>();

        for (String token : tokenList) {
            if (isNumeric(token)) {
                queue.add(token);
            } else {
                Operator operator1 = parse(token);
                while (!operatorStack.isEmpty()) {
                    Operator operator2 = operatorStack.peek();
                    if(shouldPop(operator1, operator2)) {
                        operatorStack.pop();
                        queue.add(operator2.getToken());
                    } else {
                        break;
                    }
                }
                operatorStack.push(operator1);
            }
        }

        while(!operatorStack.isEmpty()) {
            queue.add(operatorStack.pop().getToken());
        }

        return queue;
    }

    /**
     * Checks whether the input is a numeric value. It is used in the
     * convertToRpn method to separate operators and operands
     * @param str input value
     * @return true if numeric, false if not
     */
    static boolean isNumeric(String str) {
        try {
            Integer d = Integer.parseInt(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Compares the first and second operators and based on precedence and
     * the right-left associative parameters part of the shunting yard
     * algorithm. The method uses other methods to check precedence and
     * association which are implemented in the operator class.
     *
     * This method is used in the compute method to successfully evaluate
     * the rpn form of the mathematical expression.
     * @param operator1
     * @param operator2
     * @return true if conditions suggest pop, false if not.
     */
    private static boolean shouldPop(Operator operator1, Operator operator2) {
        if (operator1.isRightAssociative()) {
            if (operator1.getPrecedence() < operator2.getPrecedence()) {
                return true;
            }
        } else {
            if(operator1.getPrecedence() <= operator2.getPrecedence()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Using the rpn version of the equation, the comput method
     * solves the mathematical expression. Using a case by case
     * analysis, the for loop goes through the entire expression.
     * If the element is a number, it is pushed to the result stack.
     * Since the operators are already in the right form for precedence,
     * the operator is executed using the compute method which takes into
     * account addition, subtraction, multiplication, division, and
     * exponentials as defined in the operator class. Then the result
     * of the equation is returned.
     * @param rpnQueue contains list of numbers and operators in reverse
     *                 polish notation
     * @return equation answer
     */
    private int compute(JohnQueue<String> rpnQueue) {
        JohnStack<Integer> resultStack = new JohnStack<Integer>();

        for(int i = 0; i < rpnQueue.size(); i++) {
            String token = rpnQueue.get(i);
            if(isNumeric(token)) {
                resultStack.push(Integer.parseInt(token));
            } else {
                Operator operator = parse(token);
                operator.compute(resultStack);
            }
        }

        return resultStack.pop();
    }

    /**
     * Below is a list of inner classes. It's similar to the purpose
     * of a mouse's action listener. Like an action listener, these operations
     * are immediately called upon once required. Similar to how mouse listeners
     * react to whatever mouse actions are performed, the methods react when
     * they are required in evaluating the rpm notation of the eaution. The compute
     * method within each of the classes simply links the compute method in the operator
     * to these inner classes.
     */
    private Operator PLUS = new Operator("+", 2, false) {
        @Override
        protected int compute(int o1, int o2) {
            return o1 + o2;
        }
    };

    private Operator MINUS = new Operator("-", 2, false) {
        @Override
        protected int compute(int o1, int o2) {
            return o1 - o2;
        }
    };

    private Operator MUL = new Operator("*", 3, false) {
        @Override
        protected int compute(int o1, int o2) {
            return o1 * o2;
        }
    };

    private Operator DIV = new Operator("/", 3, false) {
        @Override
        protected int compute(int o1, int o2) {
            return o1 / o2;
        }
    };

    private Operator POW = new Operator("^", 4, true) {
        @Override
        protected int compute(int o1, int o2) {
            return (int) Math.pow(o1, o2);
        }
    };

    private Operator parse(String token) {
        if (PLUS.getToken().equals(token)) {
            return PLUS;
        } else if (MINUS.getToken().equals(token)) {
            return MINUS;
        } else if (MUL.getToken().equals(token)) {
            return MUL;
        } else if (DIV.getToken().equals(token)) {
            return DIV;
        } else if (POW.getToken().equals(token)) {
            return POW;
        }
        return null;
    }
}

/**
 * The operator class is specially designed for the calculator class to
 * deal with operator symbols (+, -, *, /, ^). Each operator is stored
 * inside a String instance. In addition, each operator has a precedence
 * associated with it (based on PEMDAS) and rightAssociative. The rightAssociative
 * tells us how to evaluate consecutive operations with the same precedence.
 * For example, the plus/minus/multiply/divide operations are all leftAssociative
 * because if given an expression (i.e. 1+2-4+6 or 5*6/7*9) the equation would
 * be calculated by executing the leftmost pair first and then moving on from there.
 * On the other hand, the exponential (i.e. 2^3^4) must have 3^4 evaluated first
 * before moving on to 2^81.
 *
 * This class is mainly used when implementing the operational inner classes
 * in the calculator class.
 */
abstract class Operator {
    private final String token;
    private final int precedence;
    private final boolean rightAssociative;

    /**
     * Constructor
     * @param token - the operator symbol
     * @param precedence - its importance. Greater # ~ more important
     * @param rightAssociative - direction with which same-precedence operations
     *                          should be completed.
     */
    protected Operator(String token, int precedence, boolean rightAssociative) {
        this.token = token;
        this.precedence = precedence;
        this.rightAssociative = rightAssociative;
    }

    /**
     * Retrieval methods for token, precedence, and association. Used in the
     * shouldPop method.
     */
    public String getToken() {
        return token;
    }

    public int getPrecedence() {
        return precedence;
    }

    public boolean isRightAssociative() {
        return rightAssociative;
    }

    @Override
    public String toString() {
        return token;
    }

    /**
     * The compute method is key in that it executes the actual computation.
     * In this method, we see that the compute method will refer to any of the
     * five operational inner classes in the calculator class. The compute method
     * knows which inner class compute to call because, if we refer to line 175-176,
     *
     * Operator operator = parse(token);
     * operator.compute(resultStack);
     *
     * We can see that the operator will be the symbol itself. When the symbol
     * calls for the method compute, based on polymorphism, the compute will automatically
     * know which compute the operator is asking for thanks to its token signature.
     */
    public int compute(JohnStack<Integer> stack) {
        // pop 2 operands off the stack
        int token1 = stack.pop();
        int token2 = stack.pop();
        // compute
        int result = compute(token2, token1);

        // push result back to stack
        stack.push(result);
        System.out.println(token2 + " " + getToken() + " " + token1 + " = " + result);
        return result;
    }

    /**
     * Makes sure that operators are implemented. A concrete implementation
     * cannot be instantiated here because it cannot account for the different
     * operations of +, -, *, /, and ^. This abstract methods basically
     * forces any operator subclass to honor its requirement of defining what
     * the operation does.
     */
    protected abstract int compute(int o1, int o2);
}

//Linked List implementation of stack class.
class JohnStack<E> {
    /**
     * The class consists of two variables: the LinkedList and the
     * size of the linked list. The Linked List is important for the
     * storage of data while the size is used to keep track of the actual
     * number of elements in the LinkedList.
     */
    private LinkedList<E> stack;

    /**
     * The Constructor simply initializes the stack and size variables
     */
    public JohnStack() {
        stack = new LinkedList<E>();
    }

    /**
     * The isEmpty method checks whether the LinkedList is
     * empty by checking the size of the LinkedList
     *
     * @return boolean result of size is equal to 0
     */
    public boolean isEmpty() {
        return (stack.size() == 0);
    }

    /**
     * The push method, which takes in a parameter,
     * adds an element to the top of the stack. This is completed
     * by the addFirst method of the LinkedList class. In addition,
     * the size is incremented.
     *
     * @param e
     */
    public void push(E e) {
        stack.addFirst(e);
    }

    /**
     * The pop method removes the top element if the list is not empty.
     * In addition, the size is decremented by one. If the list is
     * empty, then the EmptyStackException error is thrown.
     *
     * @return the removed element
     */
    public E pop() {
        if (!isEmpty()) {
            return stack.removeFirst();
        } else {
            throw new EmptyStackException();
        }
    }

    /**
     * The peek value allows the user to know the
     * value of the first element of the stack without
     * removing it like pop does.
     *
     * @return the first element in the stack
     */
    public E peek() {
        return stack.getFirst();
    }
}

/**
 * Array implementation of queue class.
 * @param <T>
 */
class JohnQueue<T> {
    private T[] array;
    private int realSize;

    private final int DEFAULT_LENGTH = 1000;

    public JohnQueue() {
        array = (T[]) new Object[DEFAULT_LENGTH];
        realSize = 0;
    }

    /**
     * Checks whether the "queue" or array is empty
     * @return whether realSize == 0 (True means empty)
     */
    public boolean isEmpty() {
        return realSize == 0;
    }

    /**
     * The add method adds an element to the queue. Given
     * the logic of first in first out, the most recently added
     * variable would go to the end of the array. While queues are
     * supposed to extend on infinitely, for this assignment, we have
     * assumed that the queue is finite, so an error message is printed
     * if the queue does not have any space anymore.
     * @param param the object to be added to the queue
     * @return true if object can be added. Otherwise, an exception is thrown
     */
    public boolean add(T param) {
        if(realSize == array.length) {
            throw new JohnQueueException("Queue is full");
        }
        array[realSize] = param;
        realSize++;
        return true;
    }

    /**
     * The remove method removes the first variable in the queue.
     * If the array is empty, then an exception is thrown. Otherwise,
     * the removal of the element involves a reconfiguration of the
     * array where each element is moved down one increment.
     * @return the object that is removed. An exception if there
     * is no object to be removed.
     */
    public T remove() {
        if(isEmpty()) {
            throw new JohnQueueException("Element does not exist");
        }
        T returnValue = array[0];
        for(int i = 0; i < realSize - 1; i++) {
            array[i] = array[i + 1];
        }
        realSize--;
        array[realSize] = null;
        return returnValue;
    }

    /**
     * Just like the stack, the peek allows the user to
     * look at the first object on the list without removing it.
     * @return the object. Null if the list is empty.
     */
    public T peek() {
        if(isEmpty()) {
            return null;
        }
        return array[0];
    }

    /**
     * Gives the user the number of elements in the queue
     * @return the actual, or real number of elements in the array
     */
    public int size() {
        return realSize;
    }

    public T get(int index) {
        return array[index];
    }
}

class JohnQueueException extends RuntimeException {
    public JohnQueueException(String message) {
        super(message);
    }
}
